/* global _ */
/* eslint-disable no-console */
/*
 * ATRraction - JS library for creating/editing *.atr files (8 bit ATARI disk images)
 *
 * author: bocianu@gmail.com
 * https://gitlab.com/bocianu/atraction
 *
 */
const
    DIRFLAG = {
        OPENED_FOR_OUTPUT: 1,
        DOS2: 2,
        DIR: 0x10,
        LOCKED: 0x20,
        NORMAL: 0x40,
        DELETED: 0x80
    },
    SECTOR_USED = 0,
    SECTOR_FREE = 1,
    ROOT_DIR = 361,
    SEC_SIZE = { SD: 0x80, DD: 0x100 },
    DOS_TYPE = { DOS2: 0, MYDOS2: 1, XBIOS: 10, UNKNOWN: 0xff },
    DOS_NAME = { 
        [DOS_TYPE.DOS2]: 'Dos II', 
        [DOS_TYPE.MYDOS2]: 'MyDos', 
        [DOS_TYPE.XBIOS]: 'xBios', 
        [DOS_TYPE.UNKNOWN]: 'None' 
    }, // eslint-disable-line no-unused-vars
    SECTOR_COUNT = {
        [DOS_TYPE.UNKNOWN]: 720,
        [DOS_TYPE.DOS2]: 720,
        [DOS_TYPE.XBIOS]: 720,
        [DOS_TYPE.MYDOS2]: 720
    },
    SECTOR_TYPE_DATA = 0,
    SECTOR_TYPE_BOOT = 1,
    SECTOR_TYPE_VTOC = 2,
    SECTOR_TYPE_DIRS = 3;

const makeAtr = function (image, secSize = SEC_SIZE.DD, dosType = DOS_TYPE.MYDOS2) { // eslint-disable-line no-unused-vars

    const dirEntry = {
        b_dirFlag: 0,
        w_size: 0,
        w_start: 0,
        a_11_name: ''
    };

    const dataSector = {
        a_125_data: [],
        a_253_data: [],
        w_parentDir: 0,
        w_next: 0,
        b_length: 0
    };

    const disk = {
        dataSectors: [],
        dos: dosType,
        sectorCount: SECTOR_COUNT[dosType],
        atrHeader: {
            w_Magic: 0x0296, // $0296 (sum of 'NICKATARI')
            w_Pars: Math.floor(((3 * 0x80) + (SECTOR_COUNT[dosType] - 3) * secSize) / 0x10), // size of this disk image, in paragraphs (size/$10)
            w_SecSize: secSize, // sector size. ($80 or $100) bytes/sector
            b_ParsHigh: 0, // high part of size, in paragraphs (added by REV 3.00)
            d_CRC: 0, // 32bit CRC of file (added by APE?)
            d_Unused: 0,
            b_Flags: 0 // bit 0 (ReadOnly) (added by APE?)
        },
        bootSector: {
            b_unused: 0,
            b_length: 3,
            w_loadAddress: 0x700,
            w_initAddress: 0x700,
            a_378_data: []
        },
        vtocPosition: 0,
        vtoc: {
            b_DosCode: 2,
            w_TotalSectors: (SECTOR_COUNT[dosType] - 12) - ((dosType != DOS_TYPE.DOS2) ? 1 : 0),
            w_FreeSectors: (SECTOR_COUNT[dosType] - 12) - ((dosType != DOS_TYPE.DOS2) ? 1 : 0),
            a_5_unused: [],
            a_118_SectorBitmap: [],
        },
        freepointer: 4,
        currentDir: {
            start: ROOT_DIR,
            dosName: ''
        },
        parentDirs: [],
        dirs: []
    };

    // ******************************************** UTILS

    const markDirSectors = startSector => {
        for (let sec = startSector; sec < startSector + 8; sec++) {
            if (!_.includes(disk.dirs, sec)) disk.dirs.push(sec);
        }
    };

    const parseBin = (uobj, prop) => {
        const aprops = _.castArray(prop);
        let result = [];
        for (let aprop of aprops) {
            if (_.isUndefined(uobj[aprop])) return null;
            if (_.startsWith(aprop, 'b_')) result.push(uobj[aprop] & 0xFF);
            if (_.startsWith(aprop, 'w_')) {
                result.push(uobj[aprop] & 0xFF);
                result.push((uobj[aprop] >> 8) & 0xFF);
            }
            if (_.startsWith(aprop, 'd_')) {
                result.push(uobj[aprop] & 0xFF);
                result.push((uobj[aprop] >> 8) & 0xFF);
                result.push((uobj[aprop] >> 16) & 0xFF);
                result.push((uobj[aprop] >> 24) & 0xFF);
            }
            if (_.startsWith(aprop, 'a_')) result = _.concat(
                result,
                _.assign(_.fill(new Array(parseInt(_.split(aprop, '_')[1])), 0), uobj[aprop])
            );
        }
        return result;
    }

    const parseLEword = (arr, idx) => arr[idx] + (arr[idx + 1] << 8);

    const parseWord = (arr, idx) => (arr[idx] << 8) + arr[idx + 1];

    const parseDouble = (arr, idx) => (arr[idx] << 24) + (arr[idx + 1] << 16) + (arr[idx + 2] << 8) + arr[idx + 3];

    const getNextFreeSector = (from) => {
        let sector = from || 4;
        while (sector < disk.sectorCount) {
            if (getVtocBitmap(sector) == SECTOR_FREE) return sector;
            sector++;
        }
        return null;
    }

    const nextFreeDirEntry = () => {
        for (let i = 0; i < 64; i++) {
            const entry = getVtocEntry(i);
            if (entry.b_dirFlag == 0) return i;
        }
        return null;
    }

    const getEntryLocation = idx => {
        return {
            sectorNum: disk.currentDir.start + Math.floor(idx / 8),
            sectorOffset: (idx % 8) * 16
        }
    }

    const getBitLocation = sectorNum => {
        return {
            byteOffset: Math.floor(sectorNum / 8),
            bitOffset: sectorNum % 8
        }
    }

    const getFreeSectorsFromBitmap = () => {
        let free = 0;
        for (let sec = 1; sec <= disk.sectorCount; sec++) {
            if (getVtocBitmap(sec) == SECTOR_FREE) free++;
        }
        return free;
    }

    const isAtrName = (name) => {
        if (name.length != 11) return false;
        let i = 0;
        while (i <= name.length) {
            const c = name.charAt(i);
            if (c == '.') return false;
            if (c != c.toUpperCase()) return false;
            i++;
        }
        return true;
    }

    const fileName2Atr = name => {
        if (isAtrName(name)) return name;
        const nameParts = _.split(name, '.');
        const atrname = nameParts[0] || '';
        const atrext = nameParts[1] || '';
        return _.padEnd(atrname.toUpperCase().substr(0, 8), 8) + _.padEnd(atrext.toUpperCase(), 3);
    }

    const atr2FileName = name => {
        if (!isAtrName(name)) return name;
        return `${_.trim(name.substr(0, 8))}${_.trim(name.substr(8, 3)).length > 0 ? '.' : ''}${_.trim(name.substr(8, 3))}`;
    }

    const objectifyDirEntry = entry => {
        const entryObj = _.merge(_.mapKeys(entry, (v, k) => _.last(_.split(k, '_'))), {
            dosName: atr2FileName(entry.a_11_name),
            isFile: (entry.b_dirFlag & (DIRFLAG.NORMAL | DIRFLAG.DELETED)) != 0,
            isDir: (entry.b_dirFlag & DIRFLAG.DIR) != 0,
            isDeleted: (entry.b_dirFlag & DIRFLAG.DELETED) != 0,
            isLocked: (entry.b_dirFlag & DIRFLAG.LOCKED) != 0,
            binData: []
        });
        if (entryObj.isFile) entryObj.binData = getFileBinary(entry.w_start, entry.w_size, entry.a_11_name);
        return entryObj;
    }

    const parseBootSector = bootbin => {
        disk.bootSector.b_unused = bootbin[0];
        disk.bootSector.b_length = bootbin[1];
        disk.bootSector.w_loadAddress = parseLEword(bootbin, 2);
        disk.bootSector.w_initAddress = parseLEword(bootbin, 4);
        disk.bootSector.a_378_data = _.slice(bootbin, 6);
    }

    
    // ******************************************** SETTERS


    const storeRawSector = (snum, sdata) => {
        const sectorSize = snum > 4 ? disk.atrHeader.w_SecSize : 0x80; // 3 first sectors MUST be ALWAYS 0x80 bytes long !!!
        disk.dataSectors[snum] = _.assign(_.fill(new Array(sectorSize), 0), sdata);
    }

    const storeBootSector = () => {
        const bootdata = _.concat(
            parseBin(disk.bootSector, [
                'b_unused',
                'b_length',
                'w_loadAddress',
                'w_initAddress',
                'a_378_data'
            ])
        );
        const bootSectorSize = 0x80;
        storeRawSector(1, _.slice(bootdata, 0, bootSectorSize));
        setVtocBitmap(1, SECTOR_USED);
        storeRawSector(2, _.slice(bootdata, bootSectorSize, bootSectorSize * 2));
        setVtocBitmap(2, SECTOR_USED);
        storeRawSector(3, _.slice(bootdata, bootSectorSize * 2, bootSectorSize * 3));
        setVtocBitmap(3, SECTOR_USED);
    };

    const storeDataSector = (sdata, parent, next) => {
        let raw = [];

        if (disk.atrHeader.w_SecSize == 0x80) {
            dataSector.a_125_data = sdata;
            raw = parseBin(dataSector, 'a_125_data');
        }
        if (disk.atrHeader.w_SecSize == 0x100) {
            dataSector.a_253_data = sdata;
            raw = parseBin(dataSector, 'a_253_data');
        }
        dataSector.w_parentDir = parent;
        dataSector.b_length = sdata.length;
        dataSector.w_next = (next) ? getNextFreeSector(disk.freepointer) : null;

        const byte1 = (dataSector.w_parentDir << 2) | (dataSector.w_next >> 8);
        const byte2 = dataSector.w_next & 0xff;

        raw = _.concat(raw, byte1, byte2, parseBin(dataSector, 'b_length'));
        storeRawSector(disk.freepointer, raw);
        setVtocBitmap(disk.freepointer, SECTOR_USED);
        disk.freepointer = getNextFreeSector();
    }

    const storeDataBlock = (sdata, parent) => {
        const blocksize = disk.atrHeader.w_SecSize - 3;
        let remain = sdata.length;
        const secSize = Math.floor(sdata.length / blocksize);
        if (secSize > disk.vtoc.w_FreeSectors) {
            throw Error('File too big to fit on disk!');
        }
        let pointer = 0;
        const start = disk.freepointer;
        let size = 0;
        while (remain > 0) {
            size = size + 1;
            remain = remain - blocksize;
            storeDataSector(_.slice(sdata, pointer, pointer + blocksize), parent, (remain > 0));
            pointer = pointer + blocksize;
        }
        return { start, size };
    }

    const storeVTOC = () => {
        if (disk.vtocPosition == 0) return null;
        disk.vtoc.w_FreeSectors = getFreeSectorsFromBitmap();
        const raw =
            parseBin(disk.vtoc, [
                'b_DosCode',
                'w_TotalSectors',
                'w_FreeSectors',
                'a_5_unused',
                'a_118_SectorBitmap'
            ])
        storeRawSector(360, raw);
        setVtocBitmap(360, SECTOR_USED);
    }

    const readVTOC = (image) => {
        return {
            b_DosCode: image[0],
            w_TotalSectors: parseLEword(image, 1),
            w_FreeSectors: parseLEword(image, 3),
            a_5_unused: _.slice(image, 5, 10),
            a_118_SectorBitmap: _.slice(image, 10, 128)
        };
    }

    const storeVtocEntry = (idx, raw) => {
        const { sectorNum, sectorOffset } = getEntryLocation(idx);
        for (let i = 0; i < 16; i++)                                     // ******** check for DD more than 16 files
            disk.dataSectors[sectorNum][sectorOffset + i] = raw[i];
    }

    const addVtocEntry = (idx, name, size, start, flag) => {
        dirEntry.b_dirFlag = flag;
        dirEntry.w_size = size;
        disk.vtoc.w_FreeSectors -= size;
        dirEntry.w_start = start;
        dirEntry.a_11_name = _.map(name, n => n.charCodeAt());
        const raw =
            parseBin(dirEntry, [
                'b_dirFlag',
                'w_size',
                'w_start',
                'a_11_name'
            ])
        storeVtocEntry(idx, raw);
    }

    const setVtocBitmap = (sectorNum, state) => {
        if (disk.vtocPosition != 0) {
            const { byteOffset, bitOffset } = getBitLocation(sectorNum);
            const mask = 0b10000000 >> bitOffset;
            if (state == SECTOR_USED) disk.vtoc.a_118_SectorBitmap[byteOffset] &= ~mask;
            if (state == SECTOR_FREE) disk.vtoc.a_118_SectorBitmap[byteOffset] |= mask;
        }
    }

    // ******************************************** GETTERS

    const getVtocEntry = (idx) => {
        const { sectorNum, sectorOffset } = getEntryLocation(idx);
        const sector = disk.dataSectors[sectorNum];
        const raw = _.slice(sector, sectorOffset, sectorOffset + 16);
        return {
            b_dirFlag: raw[0],
            w_size: parseLEword(raw, 1),
            w_start: parseLEword(raw, 3),
            a_11_name: _.join(_.map(_.slice(raw, 5), c => String.fromCharCode(c)), '')
        }
    }

    const getVtocBitmap = (sectorNum) => {
        const { byteOffset, bitOffset } = getBitLocation(sectorNum);
        let mask = 0b10000000 >> bitOffset;
        return (disk.vtoc.a_118_SectorBitmap[byteOffset] & mask) != 0 ? SECTOR_FREE : SECTOR_USED;
    }


    const getSectorType = secnum => {
        let secType = SECTOR_TYPE_DATA;
        if (_.includes([1, 2, 3], secnum)) secType = SECTOR_TYPE_BOOT;
        if (secnum == disk.vtocPosition) secType = SECTOR_TYPE_VTOC;
        if (_.includes(disk.dirs, secnum)) secType = SECTOR_TYPE_DIRS;
        return secType;
    }

    const getFileBinary = (start, len, name) => {
        let bin = [];
        let sector = start;
        let slist = [];
        for (let i = 0; i < len; i++) {
            slist.push(sector);
            try {
                const raws = disk.dataSectors[sector];
                const dataLen = raws[disk.atrHeader.w_SecSize - 1];
                bin = _.concat(bin, _.slice(raws, 0, dataLen));
                sector = parseWord(raws, disk.atrHeader.w_SecSize - 3) & 0b1111111111;
            } catch (e) {
                console.log(`Error reading sector: ${sector}`);
                console.log(`step/len: ${i}/${len} in file: ${name}`);
                console.log(slist);
                break;
            }
        }
        return bin;
    }

    const getImage = () => {
        return _.concat(
            parseBin(disk.atrHeader, [
                'w_Magic',
                'w_Pars',
                'w_SecSize',
                'b_ParsHigh',
                'd_CRC',
                'd_Unused',
                'b_Flags'

            ]),
            _.flatten(_.tail(disk.dataSectors))
        )
    };

    // ******************************************** DISK OPERATIONS

    const format = (dosType) => {
        disk.dos = dosType;
        disk.sectorCount = SECTOR_COUNT[dosType];
        disk.dataSectors = _.times(disk.sectorCount + 1, _.stubArray);
        disk.dataSectors.forEach((v, i) => disk.dataSectors[i] = _.times(disk.atrHeader.w_SecSize, _.constant(0)));
        disk.dataSectors[1] = _.times(128, _.constant(0));
        disk.dataSectors[2] = _.times(128, _.constant(0));
        disk.dataSectors[3] = _.times(128, _.constant(0));
        disk.vtocPosition = 360;
        disk.dirs = [];
        markDirSectors(361);
        disk.vtoc.w_TotalSectors = (SECTOR_COUNT[dosType] - 12) - ((dosType != DOS_TYPE.DOS2) ? 1 : 0);
        disk.vtoc.w_FreeSectors = (SECTOR_COUNT[dosType] - 12) - ((dosType != DOS_TYPE.DOS2) ? 1 : 0);
        disk.vtoc.a_118_SectorBitmap = _.times(90, _.constant(0xff));
        setVtocBitmap(0, SECTOR_USED);
        if (disk.dos != DOS_TYPE.DOS2) setVtocBitmap(disk.sectorCount, SECTOR_FREE);
        for (let i = 360; i < 369; i++) setVtocBitmap(i, SECTOR_USED);
        disk.freepointer = 4;
        disk.currentDir = {
            start: ROOT_DIR,
            dosName: ''
        };
        storeVTOC();
    };

    const setBoot = (bootbin) => {
        if (bootbin.length > 384) {
            throw Error('Maximum size of boot sector is 384 bytes!');
        }
        parseBootSector(bootbin)
        storeBootSector();
        storeVTOC();
    }

    const getDirContents = () => {
        if (disk.vtocPosition == 0) { throw Error('NO VTOC! probably not a DOS disk'); }
        const entries = [];
        for (let i = 0; i < 64; i++) {
            const entry = getVtocEntry(i);
            if (entry.b_dirFlag != 0) entries.push(entry);
        }
        return _.map(entries, objectifyDirEntry);
    }

    const setFlags = (idx, flags) => {
        const { sectorNum, sectorOffset } = getEntryLocation(idx);
        disk.dataSectors[sectorNum][sectorOffset] = flags;
    }

    const setFlag = (idx, flag) => {
        const { sectorNum, sectorOffset } = getEntryLocation(idx);
        disk.dataSectors[sectorNum][sectorOffset] |= flag;
    }

    const clearFlag = (idx, flag) => {
        const { sectorNum, sectorOffset } = getEntryLocation(idx);
        disk.dataSectors[sectorNum][sectorOffset] &= (0xff ^ flag);
    }

    const addFile = (data, name) => {
        if (disk.vtocPosition == 0) { throw Error('NO VTOC! probably not a DOS disk'); }
        const vtocNum = nextFreeDirEntry();
        if (_.isNull(vtocNum)) {
            throw Error('Too many files on disk, no free directory entry!');
        }
        const res = storeDataBlock(data, vtocNum);
        addVtocEntry(vtocNum, fileName2Atr(name), res.size, res.start, DIRFLAG.NORMAL | DIRFLAG.DOS2);
        storeVTOC();
    };

    const addDir = (name) => {
        if (disk.vtocPosition == 0) { throw Error('NO VTOC! probably not a DOS disk'); }
        if (disk.doc == DOS_TYPE.DOS2) {
            throw Error('This type of dos does not allow directiories');
        }
        const vtocNum = nextFreeDirEntry();
        if (_.isNull(vtocNum)) {
            throw Error('Too many files on disk, no free directory entry!');
        }
        const newDirStart = 0x178; /// ............................................................. eNSURE why?
        if (_.isNull(newDirStart)) return null
        addVtocEntry(vtocNum, fileName2Atr(name), 8, newDirStart, DIRFLAG.DIR);
        for (let i = newDirStart; i < newDirStart + 8; i++) setVtocBitmap(i, SECTOR_USED);
        storeVTOC();
    };

    const enterDir = (dir) => {
        if (disk.vtocPosition == 0) { throw Error('NO VTOC! probably not a DOS disk'); }
        if (disk.doc == DOS_TYPE.DOS2) return null;
        disk.parentDirs.push(disk.currentDir);
        disk.currentDir = dir;
    };

    const leaveDir = () => {
        if (disk.vtocPosition == 0) { throw Error('NO VTOC! probably not a DOS disk'); }
        if (disk.doc == DOS_TYPE.DOS2) return null;
        disk.currentDir = disk.parentDirs.pop();
    };

    //const deleteFile = () => { };

    //const renameFile = () => { };

    const parseAtrHeader = image => {
        const atrHeader = {
            w_Magic: parseLEword(image, 0), // $0296 (sum of 'NICKATARI')
            w_Pars: parseLEword(image, 2), // size of this disk image, in paragraphs (size/$10)
            w_SecSize: parseLEword(image, 4), // sector size. ($80 or $100) bytes/sector
            b_ParsHigh: image[6], // high part of size, in paragraphs (added by REV 3.00)
            d_CRC: parseDouble(image, 7), // 32bit CRC of file (added by APE?)
            d_Unused: parseDouble(image, 11),
            b_Flags: image[15] // bit 0 (ReadOnly) (added by APE?)
        };
        if (atrHeader.w_Magic != 0x0296) {
            throw Error(`ATR header invalid! Magic code = ${atrHeader.w_Magic.toString(16)}`);
        }
        const realSizePars = Math.floor((image.length - 16) / 0x10);
        if (atrHeader.w_Pars !== realSizePars) {
            throw Error(`ATR image size invalid! expected: ${atrHeader.w_Pars} VS readed: ${realSizePars}`);
        }
        disk.atrHeader = atrHeader;
    };
    const parseVtoc = (secNum) => {
        const vtoc = readVTOC(disk.dataSectors[secNum]);
        if (!_.includes([2, 3], vtoc.b_DosCode)) {
            //throw Error(`VTOC dos code invalid: ${vtoc.b_DosCode}`);
            console.log(`VTOC dos code invalid: ${vtoc.b_DosCode}`);
            return 0;
        }
        /* if (vtoc.w_TotalSectors!=disk.sectorCount) {
            throw Error(`VTOC sector count invalid: ${vtoc.w_TotalSectors} VS expected: ${disk.sectorCount}`);
            return 0;
        } */
        disk.vtoc = vtoc;
        return secNum;
    }

    const parseAtrData = image => {
        const sectors = _.slice(image, 16);
        const dataSectorsSize = sectors.length - (3 * 128);
        const secSize = disk.atrHeader.w_SecSize;
        if (dataSectorsSize % secSize != 0) {
            throw Error(`Data lenght invalid! Sector data size: ${dataSectorsSize} indivisible by sector size: ${secSize}`);
        }
        let sectorCount = dataSectorsSize / secSize;
        disk.sectorCount = sectorCount + 3;
        let secNum = 4;
        let secStart = 128 * 3;
        while (sectorCount > 0) {
            disk.dataSectors[secNum] = _.slice(sectors, secStart, secStart + secSize);
            secStart += secSize;
            secNum++;
            sectorCount--;
        }
        disk.dos = DOS_TYPE.UNKNOWN;
        disk.dirs = [];
        disk.currentDir = null;
        disk.vtocPosition = parseVtoc(360);
        disk.freepointer = null;
        if (disk.vtocPosition != 0) {
            markDirSectors(361);
            disk.dos = DOS_TYPE.MYDOS2;
            disk.freepointer = getNextFreeSector(4);
            disk.currentDir = {
                start: ROOT_DIR,
                dosName: ''
            };
        }
        setBoot(_.slice(sectors, 0, 128 * 3));
    }


    if (image) {
        format();
        parseAtrHeader(image);
        parseAtrData(image);
    }

    return {
        getSector: num => disk.dataSectors[num],
        setSector: storeRawSector,
        getSectorCount: () => disk.sectorCount,
        getSectorSize: () => disk.atrHeader.w_SecSize,
        setSectorSize: (size) => {disk.atrHeader.w_SecSize = size},
        getDosType: () => disk.dos,
        getTotalSectors: () => disk.vtoc.w_TotalSectors,
        getFreeSectors: () => disk.vtoc.w_FreeSectors,
        getSectorType,
        getImage,
        format,
        addFile,
        addDir,
        enterDir,
        leaveDir,
        getCurrentDir: () => disk.currentDir,
        currentPath: () => `${_.join(_.map(disk.parentDirs, d => d.dosName), '/')}/${disk.currentDir.dosName}`,
        getParentDirs: () => disk.parentDirs,
        getDirContents,
        //deleteFile,
        //renameFile,
        setFlags,
        setFlag,
        clearFlag,
        setBoot,
        isSectorOccupied: sec => getVtocBitmap(sec) == SECTOR_USED,
        setSectorOccupation: setVtocBitmap,
    }
}
